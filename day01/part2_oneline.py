import re

in_ = open('inputs/user.txt').read()

print(sum([int([m if m in '0123456789' else { 'zero': '0', 'one': '1', 'two': '2', 'three': '3', 'four': '4', 'five': '5', 'six': '6', 'seven': '7', 'eight': '8', 'nine': '9' } [m] for m in re.findall(r'(?=([0-9]|one|two|three|four|five|six|seven|eight|nine))', line)][0] + [m if m in '0123456789' else { 'zero': '0', 'one': '1', 'two': '2', 'three': '3', 'four': '4', 'five': '5', 'six': '6', 'seven': '7', 'eight': '8', 'nine': '9' }[m] for m in re.findall(r'(?=([0-9]|one|two|three|four|five|six|seven|eight|nine))', line)][-1]) for line in in_.splitlines()]))
