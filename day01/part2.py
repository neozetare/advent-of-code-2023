import re

in_ = open('inputs/user.txt').read()

WORD_TO_DIGIT = {'zero': '0', 'one': '1', 'two': '2', 'three': '3', 'four': '4', 'five': '5', 'six': '6', 'seven': '7',
                 'eight': '8', 'nine': '9'}

sum_ = 0

for line in in_.splitlines():
    matches = re.findall(r'(?=([0-9]|one|two|three|four|five|six|seven|eight|nine))', line)
    digits = [m if m in '0123456789' else WORD_TO_DIGIT[m] for m in matches]
    sum_ += int(digits[0] + digits[-1])

print(sum_)
