in_ = open('inputs/user.txt').read()

print(sum([int([c for c in line if c in '0123456789'][0] + [c for c in line if c in '0123456789'][-1]) for line in in_.splitlines()]))
