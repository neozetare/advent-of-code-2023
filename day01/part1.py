in_ = open('inputs/user.txt').read()

sum_ = 0

for line in in_.splitlines():
    digits = [c for c in line if c in '0123456789']
    sum_ += int(digits[0] + digits[-1])

print(sum_)
