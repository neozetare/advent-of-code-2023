in_ = open('inputs/input.txt').read()

sum_ = 0

for line in in_.splitlines():
    part1, part2 = line.split('|')
    winning_numbers = set(int(n) for n in part1.split()[2:])
    numbers_you_have = set(int(n) for n in part2.split())

    wins = len(winning_numbers.intersection(numbers_you_have))
    if wins > 0:
        sum_ += 2 ** (wins - 1)

print(sum_)
