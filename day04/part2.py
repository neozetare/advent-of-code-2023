in_ = open('inputs/input.txt').read()

scratchcards = {i: 1 for i in range(len(in_.splitlines()))}

for card_number, line in enumerate(in_.splitlines(), 1):
    part1, part2 = line.split('|')
    winning_numbers = set(int(n) for n in part1.split()[2:])
    numbers_you_have = set(int(n) for n in part2.split())

    wins = len(winning_numbers.intersection(numbers_you_have))
    for i in range(wins):
        scratchcards[card_number + 1 + i] += scratchcards[card_number]

print(sum(scratchcards.values()))