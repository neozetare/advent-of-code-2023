in_ = """Time:        54     70     82     75
Distance:   239   1142   1295   1253"""

res = 1

# part 1
#races = list(zip(*[[int(n) for n in p.split()[1:]] for p in in_.split('\n')]))

# part 2
races = [[int(''.join(p.split()[1:])) for p in in_.split('\n')]]

for time_, distance in races:
    limit_left = 0
    limit_right = time_
    
    limit_center = time_ // 2
    while limit_center - limit_left > 1:
        tested_index = (limit_center - limit_left) // 2 + limit_left
        #print('test', tested_index, tested_index * (time_ - tested_index))
        if tested_index * (time_ - tested_index) > distance:
            limit_center = tested_index
        else:
            limit_left = tested_index
        #print('limits', limit_left, limit_center)
    limit_left = limit_center
    #print('--')
    
    limit_center = time_ // 2
    while limit_right - limit_center > 1:
        tested_index = (limit_right - limit_center) // 2 + limit_center
        #print('test', tested_index, tested_index * (time_ - tested_index))
        if tested_index * (time_ - tested_index) > distance:
            limit_center = tested_index
        else:
            limit_right = tested_index
        #print('limits', limit_center, limit_right)
    limit_right = limit_center
    
    #print(limit_right - limit_left + 1)
    #print('-------')
    
    res *= limit_right - limit_left + 1

print(res)
