from collections import defaultdict, namedtuple
import re
from functools import reduce

in_ = open('inputs/input.txt').read()

Number = namedtuple('Number', ['value', 'y', 'x'])

numbers = defaultdict(lambda: {})
symbols = defaultdict(lambda: defaultdict(lambda: False))

for y, line in enumerate(in_.splitlines()):
    match_iter = re.finditer(r'\d+', line)
    for m in match_iter:
        n = Number(int(m.group()), y, m.start())
        for x in range(m.start(), m.end()):
            numbers[y][x] = n

sum_ = 0

for y, line in enumerate(in_.splitlines()):
    for x, char in enumerate(line):
        if char == '*':
            adjacent_parts = set()

            for offset_y, offset_x in [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]:
                try:
                    adjacent_parts.add(numbers[y + offset_y][x + offset_x])
                except KeyError:
                    pass

            if len(adjacent_parts) == 2:
                sum_ += reduce(lambda a, b: a * b, [p.value for p in adjacent_parts])

print(sum_)
