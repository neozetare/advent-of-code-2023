import re

in_ = open('inputs/user.txt').read()

LINE_PATTERN = re.compile(r'Game (?P<id>\d+): (?P<sets>.+)')
MAX_CUBES = {'red': 12, 'green': 13, 'blue': 14}

sum_ = 0

for line in in_.splitlines():
    match = LINE_PATTERN.match(line)
    id_ = match.group('id')

    sets = [{g.split(' ')[1]: int(g.split(' ')[0]) for g in s.split(', ')} for s in match.group('sets').split('; ')]

    for set_ in sets:
        for color, amount in set_.items():
            if amount > MAX_CUBES[color]:
                break
        else:
            continue
        break
    else:
        sum_ += int(id_)

print(sum_)
