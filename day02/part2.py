import re

in_ = open('inputs/user.txt').read()

LINE_PATTERN = re.compile(r'Game (?P<id>\d+): (?P<sets>.+)')

sum_ = 0

for line in in_.splitlines():
    match = LINE_PATTERN.match(line)
    id_ = match.group('id')

    sets = [{g.split(' ')[1]: int(g.split(' ')[0]) for g in s.split(', ')} for s in match.group('sets').split('; ')]

    min_cubes = {'red': 0, 'green': 0, 'blue': 0}

    for set_ in sets:
        for color, amount in set_.items():
            min_cubes[color] = max(min_cubes[color], amount)

    sum_ += min_cubes['red'] * min_cubes['green'] * min_cubes['blue']

print(sum_)
